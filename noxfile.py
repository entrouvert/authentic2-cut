import shlex
from pathlib import Path

import nox

nox.options.reuse_venv = True


ENV = {
    'AUTHENTIC2_SETTINGS_FILE': 'tests/settings.py',
    'DJANGO_SETTINGS_MODULE': 'authentic2.settings',
}


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def get_lasso3(session):
    src_dir = Path('/usr/lib/python3/dist-packages/')
    venv_dir = Path(session.virtualenv.location)
    for dst_dir in venv_dir.glob('lib/**/site-packages'):
        files_to_link = [src_dir / 'lasso.py'] + list(src_dir.glob('_lasso.cpython-*.so'))

        for src_file in files_to_link:
            dst_file = dst_dir / src_file.name
            if dst_file.exists():
                dst_file.unlink()
            session.log('%s => %s', dst_file, src_file)
            dst_file.symlink_to(src_file)


def install_authentic(session):
    authentic_path = Path(__file__).parent.parent / 'authentic'
    if authentic_path.exists():
        return session.install('-e', authentic_path, silent=False)
    return session.install(
        'https://git.entrouvert.org/entrouvert/authentic/archive/main.tar.gz', silent=False
    )


def setup_venv(session, *packages, django_version='>=4.2,<4.3'):
    packages = [
        f'django{django_version}',
        'djangorestframework>=3.14,<3.15',
        'django-tables2==2.4.1',
        'django-webtest',
        'file-magic',
        'ldaptools',
        'Markdown<3',
        'psycopg2-binary',
        'pyquery',
        'pytest',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    install_authentic(session)
    session.install('-e', '.', *packages, silent=False)
    get_lasso3(session)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session()
def tests(session):
    setup_venv(
        session,
        'pytest-cov',
        'pytest-django',
        'pytest-freezer',
        'pytest-random',
    )

    args = ['pg_virtualenv', '-o', 'fsync=off', 'py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--cov=src/',
            '--cov-config',
            '.coveragerc',
            '--cov-context=test',
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--junitxml=junit-django.xml',
        ]

    args += session.posargs + ['tests/']
    hookable_run(
        session,
        *args,
        env=ENV,
        external=True,
    )
    hookable_run(session, str(Path(__file__).parent / 'check-migrations.sh'), env=ENV, external=True)


@nox.session()
def check_migrations(session):
    setup_venv(session)
    hookable_run(session, str(Path(__file__).parent / 'check-migrations.sh'), env=ENV, external=True)


@nox.session()
def pylint(session):
    setup_venv(
        session,
        'nox',
        'pylint',
        'pylint-django',
    )
    pylint_command = ['pylint', '-f', 'parseable', '--rcfile', 'pylint.rc']

    if not session.posargs:
        pylint_command += ['src/authentic2_cut/', 'noxfile.py']
    else:
        pylint_command += session.posargs

    if not session.interactive:
        hookable_run(
            session,
            'bash',
            '-c',
            f'{shlex.join(pylint_command)} | tee pylint.out ; test $PIPESTATUS -eq 0',
            external=True,
            env=ENV,
        )
    else:
        hookable_run(
            session,
            *pylint_command,
            env=ENV,
        )


@nox.session()
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
