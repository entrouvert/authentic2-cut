#
# authentic2_cut - Authentic2 plugin for CUT
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from authentic2.models import AttributeValue


class RemoveFranceConnect:
    name = 'remove-franceconnect'
    title = 'Supprimer la liaison FranceConnect et déverrouiller les attributs vérifiés de l’usager'
    confirm = 'Êtes-vous sûr(e) ?'
    permission = 'custom_user.cut_fc_user'

    def do(self, view, request, user, *args, **kwargs):
        if user.attributes.validated and user.attributes.validation_context in (
            'FC',
            'fc',
        ):  # inconsistency in authentic2_cut.apps
            AttributeValue.objects.with_owner(user).update(verified=False)
            user.attributes.validation_context = None
            user.attributes.validation_date = None
            user.attributes.validation_partner = None
            user.attributes.validated = False
        user.fc_accounts.all().delete()

    def display(self, user, request):
        return user.fc_accounts.exists()
