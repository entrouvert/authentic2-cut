from django.contrib import admin

from . import models


class AttachmentsInline(admin.TabularInline):
    model = models.ValidationRequestAttachment


class ValidationRequestAdmin(admin.ModelAdmin):
    list_display = ('created', 'user', 'origin', 'status', 'validated')
    readonly_fields = ['created']
    inlines = [AttachmentsInline]


admin.site.register(models.ValidationRequest, ValidationRequestAdmin)
