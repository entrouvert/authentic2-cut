from authentic2.settings import DJANGO_RBAC_PERMISSIONS_HIERARCHY

PLATFORM = vars().get('PLATFORM')
if PLATFORM not in ['dev', 'test', 'prod']:
    PLATFORM = 'prod'

CORE_ATTRIBUTES = [
    'title',
    'first_name',
    'last_name',
    'birthdate',
    'birthplace',
    'birthcountry',
    'preferred_username',
    'preferred_givenname',
    'address_number',
    'address_street',
    'address_complement',
    'address_zipcode',
    'address_city',
    'address_country',
    'home_mobile_phone',
    'home_phone',
    'professional_mobile_phone',
    'professional_phone',
    'birthdepartment',
]

CROWN_ATTRIBUTES = []

# Manager
A2_MANAGER_SHOW_INTERNAL_ROLES = True
A2_MANAGER_ROLE_MEMBERS_FROM_OU = True
A2_MANAGER_USER_SEARCH_MINIMUM_CHARS = 2
A2_MANAGER_ROLES_SHOW_PERMISSIONS = True
A2_MANAGER_ROLE_MEMBERS_FROM_OU = True


A2_REQUIRED_FIELDS = ['email', 'first_name', 'last_name']
A2_PRE_REGISTRATION_FIELDS = ['first_name', 'last_name']

A2_RBAC_MANAGED_CONTENT_TYPES = ()
A2_CUT_PARTNERS = [
    {
        'domains': ['.lyon.fr'],
        'name': 'Ville de Lyon',
    },
    {
        'domains': ['.entrouvert.org'],
        'name': 'Ville de Lyon',
    },
]

# 48h pour les mails de reset de mot de passe
PASSWORD_RESET_TIMEOUT = 60 * 60 * 24 * 2
A2_EMAIL_CHANGE_TOKEN_LIFETIME = 3600 * 24 * 2
ACCOUNT_ACTIVATION_DAYS = 2


# CACHALOT_UNCACHABLE_TABLES = ('custom_user_user', 'django_migrations')

# OIDC
A2_IDP_OIDC_SCOPES = ['openid', 'email', 'profile', 'crown']

A2_FC_USER_INFO_MAPPINGS = {
    'first_name': 'given_name',
    'last_name': 'family_name',
    'birthdate': {'ref': 'birthdate', 'translation': 'isodate'},
    'birthplace': {'ref': 'birthplace', 'translation': 'insee-communes'},
    'birthcountry': {'ref': 'birthcountry', 'translation': 'insee-countries'},
    'birthplace_insee': 'birthplace',
    'birthcountry_insee': 'birthcountry',
    'title': {
        'ref': 'gender',
        'translation': 'simple',
        'translation_simple': {
            'male': 'Monsieur',
            'female': 'Madame',
        },
    },
    'preferred_username': {
        'ref': 'preferred_username',
        'if-empty': True,
    },
    'email': {
        'ref': 'email',
        'if-empty': True,
        'tag': 'email',
    },
    'email_verified': {
        'ref': 'email',
        'translation': 'notempty',
        'if-tag': 'email',
    },
}

A2_FC_SCOPES = ['openid', 'identite_pivot', 'email']

TEMPLATE_VARS = {
    'help_url': 'https://support.grandlyon.com/glc/',
    'account_label': 'compte GRANDLYON CONNECT',
    'partners_url': 'https://www.grandlyon.com/projets/metropole-intelligente.html',
    'support_url': 'https://support.grandlyon.com/glc/#generalites',
}
if PLATFORM == 'dev':
    TEMPLATE_VARS.update(
        {
            'environment_label': 'Site de développement',
            'moncompte_url': 'https://connexion-grandlyon.dev.entrouvert.org',
            'help_url': 'https://portail-citoyen-sau.guichet-recette.grandlyon.com/glc/',
        }
    )
elif PLATFORM == 'test':
    TEMPLATE_VARS.update(
        {
            'environment_label': 'Site de test',
            'smarttag_js': 'https://tag.aticdn.net/586793/smarttag.js',
            'visits_tracking_js': 'var tag = new ATInternet.Tracker.Tag();',
            'moncompte_url': 'https://moncompte-rec.grandlyon.com',
            'help_url': 'https://portail-citoyen-sau.guichet-recette.grandlyon.com/glc/',
        }
    )
elif PLATFORM == 'prod':
    TEMPLATE_VARS.update(
        {
            'smarttag_js': 'https://tag.aticdn.net/586794/smarttag.js',
            'visits_tracking_js': 'var tag = new ATInternet.Tracker.Tag();',
            'moncompte_url': 'https://moncompte.grandlyon.com',
        }
    )

A2_HOMEPAGE_URL = '/accounts/'
A2_MANAGER_SITE_TITLE = 'GRANDLYON CONNECT'
A2_LOGIN_REDIRECT_AUTHENTICATED_USERS_TO_HOMEPAGE = True

DJANGO_RBAC_PERMISSIONS_HIERARCHY['cut_validate'] = ['view', 'search']
DJANGO_RBAC_PERMISSIONS_HIERARCHY['cut_fc'] = ['view', 'search']
THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.convert_engine.Engine'
THUMBNAIL_FORCE_OVERWRITE = False

A2_OPENED_SESSION_COOKIE_NAME = 'GLC_OPENED_SESSION'
