import os

from authentic2_idp_oidc.models import OIDCClient
from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand
from django.db.transaction import atomic

from authentic2_cut.models import ValidationRequest, ValidationRequestAttachment


class Command(BaseCommand):
    help = 'Create validation requests'

    def add_arguments(self, parser):
        parser.add_argument('--ou-slug')
        parser.add_argument('--oidc-client-slug')
        parser.add_argument('--user-pk', type=int)
        parser.add_argument('paths', nargs='+')

    def handle(self, ou_slug, oidc_client_slug, user_pk, paths, **options):
        oidc_client = OIDCClient.objects.get(ou__slug=ou_slug, slug=oidc_client_slug)
        User = get_user_model()
        user = User.objects.get(pk=user_pk)

        with atomic():
            validation_request = ValidationRequest.objects.create(user=user, origin=oidc_client)
            for path in paths:
                with open(path) as file_object:
                    filename = os.path.basename(path)
                    f = ContentFile(file_object.read(), name=filename)
                    ValidationRequestAttachment.objects.create(validation_request=validation_request, image=f)
        self.stdout.write('New validation request %s' % validation_request.pk)
