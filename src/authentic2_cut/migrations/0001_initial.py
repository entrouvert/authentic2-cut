from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Journal',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'timestamp',
                    models.DateTimeField(auto_now_add=True, verbose_name='Horodatage', db_index=True),
                ),
                ('message', models.TextField(verbose_name='Message')),
                (
                    'actor',
                    models.ForeignKey(
                        related_name='actor_journal',
                        verbose_name='Auteur',
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.SET_NULL,
                    ),
                ),
                (
                    'subject',
                    models.ForeignKey(
                        related_name='subject_journal',
                        verbose_name='Sujet',
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.SET_NULL,
                    ),
                ),
            ],
            options={
                'ordering': ('-timestamp', '-id'),
                'verbose_name': 'historique',
                'verbose_name_plural': 'historiques',
            },
        ),
    ]
