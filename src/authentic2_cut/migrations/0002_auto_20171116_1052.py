from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2_cut', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='subject',
            field=models.ForeignKey(
                related_name='subject_journal',
                verbose_name='Sujet',
                to=settings.AUTH_USER_MODEL,
                null=True,
                on_delete=models.SET_NULL,
            ),
        ),
    ]
