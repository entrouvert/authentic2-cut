from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('authentic2_cut', '0002_auto_20171116_1052'),
    ]

    operations = [
        migrations.CreateModel(
            name='ValidationRequest',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'created',
                    models.DateTimeField(
                        auto_now_add=True, verbose_name='Date de cr\xe9ation', db_index=True
                    ),
                ),
                ('origin_id', models.PositiveIntegerField(verbose_name='origin id')),
                (
                    'status',
                    models.CharField(
                        default='received',
                        max_length=16,
                        verbose_name='Statut',
                        choices=[
                            ('received', 're\xe7ue'),
                            ('accepted', 'accept\xe9e'),
                            ('refused', 'refus\xe9e'),
                        ],
                    ),
                ),
                (
                    'reason',
                    models.TextField(
                        blank=True,
                        verbose_name='Raison du refus',
                        choices=[
                            ('unreadable', 'pi\xe8ce(s) illisible(s)'),
                            ('invalid', 'pi\xe8ce(s) invalides(s)'),
                            ('underaged', 'invividu mineur'),
                        ],
                    ),
                ),
                ('validated', models.DateTimeField(null=True, verbose_name='Date de validation')),
                ('external_id', models.TextField(null=True, verbose_name='Identifiant externe', blank=True)),
                ('taken', models.DateTimeField(null=True, verbose_name='En cours')),
                (
                    'origin_ct',
                    models.ForeignKey(
                        verbose_name='origin ct', to='contenttypes.ContentType', on_delete=models.CASCADE
                    ),
                ),
                (
                    'taken_by',
                    models.ForeignKey(
                        related_name='validation_requests_taken',
                        verbose_name='Trait\xe9 par',
                        to=settings.AUTH_USER_MODEL,
                        null=True,
                        on_delete=models.SET_NULL,
                    ),
                ),
                (
                    'user',
                    models.ForeignKey(
                        related_name='validation_requests',
                        verbose_name='Utilisateur',
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    'validated_by',
                    models.ForeignKey(
                        related_name='validation_requests_validated',
                        verbose_name='Valid\xe9/refus\xe9 par',
                        to=settings.AUTH_USER_MODEL,
                        null=True,
                        on_delete=models.SET_NULL,
                    ),
                ),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name': 'Requ\xeate de validation',
                'verbose_name_plural': 'Requ\xeates de validation',
            },
        ),
        migrations.CreateModel(
            name='ValidationRequestAttachment',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('image', models.ImageField(upload_to='', verbose_name='contenu')),
                (
                    'validation_request',
                    models.ForeignKey(
                        related_name='attachments',
                        verbose_name='requ\xeate de validation',
                        to='authentic2_cut.ValidationRequest',
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                'ordering': ('pk',),
                'verbose_name': 'Pi\xe8ce jointe',
                'verbose_name_plural': 'Pi\xe8ces jointes',
            },
        ),
        migrations.AlterUniqueTogether(
            name='validationrequest',
            unique_together={('origin_ct', 'origin_id', 'user', 'external_id')},
        ),
    ]
