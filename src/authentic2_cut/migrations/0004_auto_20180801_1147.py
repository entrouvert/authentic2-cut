from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('authentic2_cut', '0003_auto_20180423_1532'),
    ]

    operations = [
        migrations.AlterField(
            model_name='validationrequest',
            name='reason',
            field=models.TextField(
                blank=True,
                verbose_name='Raison du refus',
                choices=[
                    ('unreadable', 'Pi\xe8ce(s) illisible(s)'),
                    ('invalid', 'Pi\xe8ce(s) invalides(s)'),
                    ('underaged', 'Invividu mineur'),
                ],
            ),
        ),
        migrations.AlterField(
            model_name='validationrequest',
            name='validated',
            field=models.DateTimeField(null=True, verbose_name='Date de validation', blank=True),
        ),
        migrations.AlterField(
            model_name='validationrequest',
            name='validated_by',
            field=models.ForeignKey(
                related_name='validation_requests_validated',
                verbose_name='Valid\xe9/refus\xe9 par',
                blank=True,
                to=settings.AUTH_USER_MODEL,
                null=True,
                on_delete=models.SET_NULL,
            ),
        ),
    ]
