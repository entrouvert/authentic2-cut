# authentic2_cut - Authentic2 plugin for CUT
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django_tables2 as tables
from django.utils.translation import gettext_lazy as _

from . import models


class ValidationTable(tables.Table):
    pk = tables.LinkColumn(
        viewname='cut-manager-user-validation', kwargs={'pk': tables.A('pk')}, verbose_name='Identifiant'
    )
    user = tables.LinkColumn(
        viewname='a2-manager-user-detail',
        kwargs={'pk': tables.A('user.pk')},
        accessor='user.get_full_name',
        order_by=('user__last_name', 'user__first_name'),
    )
    origin = tables.Column(verbose_name='Service demandeur', orderable=False)
    human_status = tables.Column(order_by=('status',), verbose_name='Statut')
    human_status_who = tables.LinkColumn(
        verbose_name='Agent',
        viewname='a2-manager-user-detail',
        accessor='human_status_who.get_full_name',
        kwargs={'pk': tables.A('human_status_who.pk')},
        orderable=False,
    )

    class Meta:
        model = models.ValidationRequest
        attrs = {'class': 'main', 'id': 'validation-request-table'}
        fields = (
            'pk',
            'created',
            'user',
            'origin',
            'external_id',
            'validated',
            'human_status',
        )
        empty_text = _('None')
