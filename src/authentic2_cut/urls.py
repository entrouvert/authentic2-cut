# authentic2_cut - Authentic2 plugin for CUT
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from authentic2.decorators import required
from authentic2.manager.utils import manager_login_required
from authentic2.views import ProfileView
from django.urls import path, re_path

from . import api_views, views

urlpatterns = required(
    manager_login_required,
    [
        path(
            'accounts/profile/',
            ProfileView.as_view(template_names=['authentic2/cut-profile.html']),
            name='cut-profile',
        ),
        path('accounts/edit-core/', views.edit_core, name='cut-edit-core'),
        path('accounts/edit-crown/', views.edit_crown, name='cut-edit-crown'),
        path('manage/users/<int:pk>/', views.manager_user_detail, name='a2-manager-user-detail'),
        re_path(
            r'^manage/users/uuid:(?P<slug>[a-z0-9]+)/$',
            views.manager_user_detail,
            name='a2-manager-user-by-uuid-detail',
        ),
        path(
            'manage/users/<int:pk>/edit-core/',
            views.manager_user_edit_core,
            name='cut-manager-user-edit-core',
        ),
        path('manage/validation/', views.validation_homepage, name='cut-manager-user-validation'),
        path('manage/validation/next/', views.next_validation, name='cut-manager-user-next-validation'),
        path('manage/validation/<int:pk>/', views.validation, name='cut-manager-user-validation'),
        re_path(
            r'^manage/validation/attachment/(?P<pk>\d*)/(?P<filename>.*)$',
            views.validation_attachment,
            name='cut-manager-user-validation-attachment',
        ),
        re_path(
            r'^manage/validation/attachment-thumbnail/(?P<pk>\d*)/(?P<filename>.*)$',
            views.validation_attachment_thumbnail,
            name='cut-manager-user-validation-attachment-thumbnail',
        ),
    ],
)

urlpatterns += [
    path('cgu/', views.cgu, name='cut-cgu'),
    path('api/validate/', api_views.validate, name='api-cut-validate'),
    path(
        'api/front-page-stats/',
        api_views.front_page_stats,
        name='api-cut-front-page-stats',
    ),
]
