# authentic2_cut - Authentic2 plugin for CUT
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.html import format_html


class FranceConnectUserData:
    def __init__(self, user, request):
        self.user = user
        self.request = request

    def __str__(self):
        from authentic2_auth_fc.models import FcAccount

        if FcAccount.objects.filter(user=self.user).exists():
            return format_html('<p>Utilisateur relié à un compte FranceConnect</p>')
        else:
            return ''


class ValidationUserData:
    def __init__(self, user):
        self.user = user

    def __str__(self):
        context_map = {
            'FC': 'par FranceConnect',
            'office': 'au guichet',
            'online': 'en ligne',
            'BO': 'au guichet',
        }
        validation_context = self.user.attributes.validation_context
        validation_date = self.user.attributes.validation_date
        if not validation_date:
            return ''
        return format_html(
            '<p id="a2-manager-user-cut-validation">Compte validé <em>{0}</em> le {1}</p>',
            context_map.get(validation_context, validation_context),
            validation_date.strftime('%d/%m/%Y'),
        )
