import copy
import time
from collections import namedtuple

import django_webtest
import pytest

try:
    import pathlib
except ImportError:
    import pathlib2 as pathlib

from authentic2.a2_rbac.models import OrganizationalUnit as OU
from django.contrib.auth import get_user_model
from django.core.management import call_command

User = get_user_model()
TEST_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def app(request, db, settings, tmpdir):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    settings.MEDIA_DIR = str(tmpdir.mkdir('media'))
    call_command('loaddata', 'cut_attributes.json')
    return django_webtest.DjangoTestApp(extra_environ={'HTTP_HOST': 'localhost', 'wsgi.url_scheme': 'https'})


@pytest.fixture
def partner_ou(db):
    return OU.objects.create(name='partner', slug='ou')


@pytest.fixture
def glc(app, partner_ou, db):
    from authentic2_idp_oidc.models import OIDCClient

    oidc_client = OIDCClient.objects.create(
        name='Client 1',
        slug='client1',
        ou=partner_ou,
        client_id='client1',
        client_secret='client1',
        # IMPORTANT !
        has_api_access=True,
        identifier_policy=OIDCClient.POLICY_PAIRWISE_REVERSIBLE,
    )
    GLC = namedtuple('GLC', ['oidc_client'])
    return GLC(oidc_client=oidc_client)


@pytest.fixture
def glc_app(app, glc):
    app = copy.copy(app)
    app.authorization = ('Basic', (glc.oidc_client.client_id, glc.oidc_client.client_secret))
    return app


class AllHook:
    def __init__(self):
        self.calls = {}
        from authentic2 import hooks

        hooks.get_hooks.cache.clear()

    def __call__(self, hook_name, *args, **kwargs):
        calls = self.calls.setdefault(hook_name, [])
        calls.append({'args': args, 'kwargs': kwargs})

    def __getattr__(self, name):
        return self.calls.get(name, [])

    def clear(self):
        self.calls = {}


@pytest.fixture
def user(db):
    user = User.objects.create(
        username='john.doe',
        email='john.doe@example.net',
        first_name='John',
        last_name='Doe',
        email_verified=True,
    )
    user.set_password('john.doe')
    return user


@pytest.fixture
def hooks(settings):
    if hasattr(settings, 'A2_HOOKS'):
        hooks = settings.A2_HOOKS
    else:
        hooks = settings.A2_HOOKS = {}
    hook = hooks['__all__'] = AllHook()
    yield hook
    hook.clear()
    del settings.A2_HOOKS['__all__']


@pytest.fixture
def admin(db):
    user = User(username='admin', email='admin@example.net', is_superuser=True, is_staff=True)
    user.ou = OU.objects.get(slug='territoire')
    user.set_password('admin')
    user.save()
    return user


@pytest.fixture
def jpeg_file():
    with (TEST_DIR / 'minimal.jpg').open('rb') as fd:
        yield fd.read()


@pytest.fixture
def png_file():
    with (TEST_DIR / 'minimal.png').open('rb') as fd:
        yield fd.read()


@pytest.fixture
def pdf_file():
    with (TEST_DIR / 'minimal.pdf').open('rb') as fd:
        yield fd.read()


@pytest.fixture(autouse=True)
def clean_caches():
    from authentic2.apps.journal.models import event_type_cache

    event_type_cache.cache.clear()
