import os

ALLOWED_HOSTS = ['localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'authentic2-cut',
    }
}

if 'postgres' in DATABASES['default']['ENGINE']:
    for key in ('PGPORT', 'PGHOST', 'PGUSER', 'PGPASSWORD'):
        if key in os.environ:
            DATABASES['default'][key[2:]] = os.environ[key]

LANGUAGE_CODE = 'en'
A2_FC_CLIENT_ID = ''
A2_FC_CLIENT_SECRET = ''

# test hook handlers
A2_HOOKS_PROPAGATE_EXCEPTIONS = True

INSTALLED_APPS += (
    'sorl.thumbnail',
    'authentic2_cut',
)
MIDDLEWARE += ('authentic2_cut.middlewares.CUTMiddleware',)
