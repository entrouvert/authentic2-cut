import base64
import datetime
import uuid

import pytest
from authentic2.a2_rbac.models import OrganizationalUnit
from authentic2_idp_oidc.models import OIDCClient
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.utils.encoding import force_str
from utils import login

from authentic2_cut import models

User = get_user_model()

JOHN = 'Jôhn'
DOE = 'Dôe'
EMAIL = 'john.doe@entrouvert.com'


@pytest.fixture
def john(glc_app):
    response = glc_app.post_json(
        '/api/users/',
        params={
            'first_name': JOHN,
            'last_name': DOE,
            'email': EMAIL,
        },
    )
    user = User.objects.get(first_name=JOHN)
    assert response.json['sub'] != user.uuid
    assert response.json['first_name'] == JOHN
    assert response.json['last_name'] == DOE
    assert response.json['email'] == EMAIL
    assert response.json['date_joined'].endswith('Z')
    assert response.json['modified'].endswith('Z')
    assert user.first_name == JOHN
    assert user.last_name == DOE
    assert user.email == EMAIL
    assert user.ou.slug == 'usagers'
    user._oidc_sub = response.json['sub']
    return user


def test_no_email(glc_app):
    app = glc_app

    response = app.post_json('/api/users/', params={}, status=400)
    assert set(response.json['errors']) == {'first_name', 'last_name', 'email'}
    assert response.json['result'] == 0


def test_create_user(john):
    assert john


def helper_test_validation_image(glc_app, john, image_file, extension):
    external_id = uuid.uuid4().hex
    response = glc_app.post_json(
        '/api/users/%s/validate/' % john._oidc_sub,
        params={
            'external_id': external_id,
            'justificatifs': [
                {
                    'b64_content': force_str(base64.b64encode(image_file)),
                }
            ],
        },
        status=201,
    )
    assert response.json == {
        'status': 'received',
        'id': response.json['id'],
        'result': 1,
        'external_id': external_id,
        'sub': john._oidc_sub,
    }
    validation_request = models.ValidationRequest.objects.get(id=response.json['id'])
    attachment = models.ValidationRequestAttachment.objects.get(validation_request=validation_request)
    assert attachment.image.name.endswith(extension)
    return response.json['id']


def test_validation_jpg(app, admin, glc_app, john, jpeg_file):
    validation_id = helper_test_validation_image(glc_app, john, jpeg_file, 'jpeg')

    response = login(app, admin, 'cut-manager-user-validation')
    response = response.click(str(validation_id))
    assert response.pyquery('.popup-image')


def test_validation_png(app, admin, glc_app, john, png_file):
    validation_id = helper_test_validation_image(glc_app, john, png_file, 'png')
    response = login(app, admin, 'cut-manager-user-validation')
    response = response.click(str(validation_id))
    assert response.pyquery('.popup-image')


def test_validation_pdf(app, admin, glc_app, john, pdf_file):
    validation_id = helper_test_validation_image(glc_app, john, pdf_file, 'pdf')
    response = login(app, admin, 'cut-manager-user-validation')
    response = response.click(str(validation_id))
    assert not response.pyquery('.popup-image')


def test_many_attachments(app, admin, glc_app, john, png_file, jpeg_file, pdf_file):
    external_id = uuid.uuid4().hex
    response = glc_app.post_json(
        '/api/users/%s/validate/' % john._oidc_sub,
        params={
            'external_id': external_id,
            'justificatifs': [
                {
                    'b64_content': force_str(base64.b64encode(png_file)),
                },
                {
                    'b64_content': force_str(base64.b64encode(jpeg_file)),
                },
                {
                    'b64_content': force_str(base64.b64encode(pdf_file)),
                },
            ],
        },
        status=201,
    )
    assert response.json == {
        'status': 'received',
        'id': response.json['id'],
        'result': 1,
        'external_id': external_id,
        'sub': john._oidc_sub,
    }
    validation_request = models.ValidationRequest.objects.get(id=response.json['id'])
    assert validation_request.attachments.count() == 3
    validation_id = str(response.json['id'])
    response = login(app, admin, 'cut-manager-user-validation')
    response = response.click(validation_id)
    assert len(response.pyquery('#attachments p a')) == 3
    attachments_elts = response.pyquery('#attachments p a')
    assert [elt.attrib.get('class', '') for elt in attachments_elts] == ['popup-image', 'popup-image', '']
    assert app.get(attachments_elts[0].attrib['href']).content == png_file
    assert app.get(attachments_elts[1].attrib['href']).content == jpeg_file
    assert app.get(attachments_elts[2].attrib['href']).content == pdf_file


def test_front_page_stats_api(glc_app, john):
    usagers, dummy = OrganizationalUnit.objects.get_or_create(name='Usagers')
    john.ou = usagers
    john.save()

    cache.clear()

    response = glc_app.get('/api/front-page-stats/')
    assert response.json['data']['users_count'] == 1
    assert response.json['data']['services_count'] == 1
    for i in range(5):
        User.objects.create(
            email=f'john-{i}@example.com',
            username=f'john-{i}',
            ou=usagers,
        )
        OIDCClient.objects.create(
            client_id=f'client-{i}',
            client_secret='auie',
            slug=f'client-{i}',
        )
    response = glc_app.get('/api/front-page-stats/')
    assert response.json['data']['users_count'] == 1
    assert response.json['data']['services_count'] == 1

    cache.clear()
    response = glc_app.get('/api/front-page-stats/')
    assert response.json['data']['users_count'] == 6
    assert response.json['data']['services_count'] == 6
