import datetime
import os.path

from django.core.files.base import ContentFile
from django.core.management import call_command

from authentic2_cut import models


def test_validation_request_cleanup(db, admin, user, glc, freezer, png_file):
    req = models.ValidationRequest.objects.create(origin=glc.oidc_client, user=user)
    attachment = models.ValidationRequestAttachment.objects.create(
        validation_request=req, image=ContentFile(png_file, 'aaa')
    )
    assert models.ValidationRequest.objects.count() == 1
    assert models.ValidationRequestAttachment.objects.count() == 1
    call_command('cleanupauthentic')
    assert models.ValidationRequest.objects.count() == 1
    assert models.ValidationRequestAttachment.objects.count() == 1

    freezer.move_to(datetime.timedelta(days=93))
    call_command('cleanupauthentic')
    assert models.ValidationRequest.objects.count() == 1
    assert models.ValidationRequestAttachment.objects.count() == 1

    req.accept(user=admin)
    call_command('cleanupauthentic')
    assert models.ValidationRequest.objects.filter(validated__isnull=False).count() == 1
    assert models.ValidationRequest.objects.count() == 1
    assert models.ValidationRequestAttachment.objects.count() == 1
    assert os.path.exists(attachment.image.path)

    freezer.move_to(datetime.timedelta(days=93))
    call_command('cleanupauthentic')
    assert models.ValidationRequest.objects.count() == 0
    assert models.ValidationRequestAttachment.objects.count() == 0
    assert not os.path.exists(attachment.image.path)
