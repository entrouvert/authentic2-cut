from authentic2.utils.misc import make_url
from django.urls import reverse


def login(app, user, path=None, password=None, remember_me=None):
    if path:
        real_path = make_url(path)
        login_page = app.get(real_path, status=302).maybe_follow()
    else:
        login_page = app.get(reverse('auth_login'))
    assert login_page.request.path == reverse('auth_login')
    form = login_page.form
    form.set('username', user.username if hasattr(user, 'username') else user)
    # password is supposed to be the same as username
    form.set('password', password or user.username)
    if remember_me is not None:
        form.set('remember_me', bool(remember_me))
    response = form.submit(name='login-password-submit').follow()
    if path:
        assert response.request.path == real_path
    else:
        assert response.request.path == reverse('auth_homepage')
    assert '_auth_user_id' in app.session
    return response
